//This program sorts number from lesser to greater

package saengsawang.pasagorn.lab2;

import java.util.Arrays;

public class IntegersSorter {

	public static void main(String[] args) {
		
		if(Integer.parseInt(args[0]) == 1) System.out.println("IntegersSorter <the number of integers to sort> <i1> <i2> ...<in>");
		
		int[] numbers = new int[Integer.parseInt(args[0])];
		
		for(int i = 0 ; i < Integer.parseInt(args[0]) ; i++){
			numbers[i] = Integer.parseInt(args[i+1]);
		}
		
		//show the numbers before sorting
		System.out.print("Before sorting: [");
		for(int i = 0 ; i < Integer.parseInt(args[0]) - 1 ; i++){
			System.out.print(numbers[i] + ", ");
		}
		System.out.println(numbers[Integer.parseInt(args[0])-1] + "]");
		
		//sort numbers
		Arrays.sort(numbers);
		
		//show the numbers before sorting
		System.out.print("After sorting: [");
		for(int i = 0 ; i < Integer.parseInt(args[0]) - 1 ; i++){
			System.out.print(numbers[i] + ", ");
		}
		System.out.print(numbers[Integer.parseInt(args[0])-1] + "]");
		

	}

}
