//This program help exchange bank note

package saengsawang.pasagorn.lab2;

public class MoneyChanger {
	
	public static void main(String[] args) {
		
		int cost = Integer.parseInt(args[0]);
		int change = 1000 - cost;
		int hundred, fifty, twenty;
		
		System.out.println("You give 1000 for the cost as " + cost + " Baht.");
		System.out.println("You will recieve exchange as " + change + " Baht.");
		
		//find the number of 500 bank note
		if(change >= 500){
			System.out.print("1 of 500 bank note; ");
			change = change % 500;			
		}
		
		//find the number of 100 bank note
		if(change >= 100){
			hundred = change / 100;
			System.out.print(hundred + " of 100 bank note; ");
			
			change = change % 100;
		}
		
		//find the number of 50 bank note
		if(change >= 50){
			fifty = change / 50;
			System.out.print(fifty + " of 50 bank note; ");
			change = change % 50;				
		}
		
		//find the number of 20 bank note
		if(change >= 20){
			twenty = change / 20;
			System.out.print(twenty + " of 20 bank note; ");
		}
		
		//if there is no change
		if(change == 0) System.out.println("No change.");	
	}

}
