package saengsawang.pasagorn.lab2;

//This program receive and show data of the patients

public class PatientV2{
	
	//Check if the Height or Weight is negative number or not 
	public static boolean isNegative(double number){
		if(number < 0) return true;
		else return false;
	}
	
	public static void main(String[] args){
		//check if the information was input correctly
		if(args.length != 4){
			System.err.println("Patient <name> <gender> <weight> <height>");
		}
		else{
			System.out.println("This patient name is " + args[0]);
			if(args[1].equalsIgnoreCase("Female")){
				if(isNegative(Float.parseFloat(args[2]))){
					System.out.println("Weight must be non-negative");				
				}
				else if(isNegative(Integer.parseInt(args[3]))){
					System.out.println("Height must be non-negative");
				}
				else{
					System.out.println("Her weight is " + Float.parseFloat(args[2]) + " kg. and height is " + Integer.parseInt(args[3]) + " cm.");
				}			
			}
			else if(args[1].equalsIgnoreCase("Male")){
				if(isNegative(Float.parseFloat(args[2]))){
					System.out.println("Weight must be non-negative");				
				}
				else if(isNegative(Integer.parseInt(args[3]))){
					System.out.println("Height must be non-negative");
				}
				else{
					System.out.println("His weight is " + Float.parseFloat(args[2]) + " kg. and height is " + Integer.parseInt(args[3]) + " cm.");
				}			
			}
			else{
				System.out.println("Please enter gender as only Male or Female");	
			}
		
		}
				
	}
}
