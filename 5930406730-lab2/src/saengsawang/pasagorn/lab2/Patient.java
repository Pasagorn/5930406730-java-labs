package saengsawang.pasagorn.lab2;

public class Patient {
	public static void main(String[] args){
			System.out.println("This patient name is " + args[0]);
			if(args[1].equals("Female")){
				System.out.println("Her weight is " + Float.parseFloat(args[2]) + " kg. and height is " + Integer.parseInt(args[3]) + " cm.");
			}
			else if(args[1].equals("Male")){
				System.out.println("His weight is " + Float.parseFloat(args[2]) + " kg. and height is " + Integer.parseInt(args[3]) + " cm.");			
			}
			else{
				System.out.println("Please enter gender as only Male or Female");	
			}
	}
}
