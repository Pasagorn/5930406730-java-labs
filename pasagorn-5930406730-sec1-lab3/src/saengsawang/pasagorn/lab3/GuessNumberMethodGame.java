package saengsawang.pasagorn.lab3;

import java.util.Scanner;

public class GuessNumberMethodGame {

	static int remainingGuess = 7;
	static int answer;

	public static void main(String args[]) {
		genAnwser();
		playGame();
	}

	private static void genAnwser() {
		answer = 0 + (int) (Math.random() * ((100 - 0) + 1));
	}

	private static void playGame() {
		int guess;
		Scanner number = new Scanner(System.in);
		while (remainingGuess > 0) {
			System.out.println("Number remaining guess is " + remainingGuess);
			System.out.print("Enter a guess: ");
			guess = number.nextInt();

			if (guess == answer) {
				System.out.print("Correct!");
				return;
			} else if (guess < answer) {
				System.out.println("Higher!");
				remainingGuess--;
			} else if (guess > answer) {
				System.out.println("lower!");
				remainingGuess--;
			}
		}
		System.out.println("You ran out of guesses. The number was " + answer);
	}

}
