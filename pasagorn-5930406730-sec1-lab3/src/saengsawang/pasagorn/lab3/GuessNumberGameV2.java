package saengsawang.pasagorn.lab3;

import java.util.Scanner;

public class GuessNumberGameV2 {

	static int remainingGuess;
	static int answer;
	static int max, min;	
	static Scanner scanNum = new Scanner(System.in);

	public static void main(String args[]) {
		Scanner scanCha = new Scanner(System.in);
		String again = null;
		do {
			genAnwser();
			playGame();
			System.out.print("\nWant to play again? ( \"Y\" or \"y\") : ");
			again = scanCha.nextLine();
		} while (again.equalsIgnoreCase("y"));
		
	}

	private static void genAnwser() {
		
		System.out.print("Enter min and max of random numbers: ");
		min = scanNum.nextInt();
		max = scanNum.nextInt();		
		answer = min + (int) (Math.random() * ((max - min) + 1));
		System.out.print("Enter the number possible guess: ");
		remainingGuess = scanNum.nextInt();
	}

	private static void playGame() {
		int guess;
		
		while (remainingGuess > 0) {
			System.out.println("Number remaining guess is " + remainingGuess);
			System.out.print("Enter a guess: ");
			guess = scanNum.nextInt();
			if (guess >= min && guess <= max) {
				if (guess == answer) {
					System.out.print("Correct!");
					return;
				} else if (guess < answer) {
					System.out.println("Higher!");
					remainingGuess--;
				} else if (guess > answer) {
					System.out.println("lower!");
					remainingGuess--;
				}
			}
			else {
				System.out.println("Number must be in " + min + " and " + max);
			}

			
		}
		System.out.println("You ran out of guesses. The number was " + answer);
	}
	

}
