package saengsawang.pasagorn.lab11;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * TetrisGame
 * 
 * This program is able to move the tetris left/right/down with arrow keys and
 * the tetris stop at edge of the window
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */

public class TetrisGame extends JFrame {

	private static final long serialVersionUID = 1L;
	TetrisPanel panel;

	public TetrisGame(String name) {
		super(name);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected static void createAndShowGUI() {
		TetrisGame tetris = new TetrisGame("CoE Tetris Game");
		tetris.addComponents();
		tetris.setFrameFeatures();
	}

	protected void addComponents() {
		panel = new TetrisPanel();
		add(panel);
	}

	protected void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setVisible(true);
		pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setResizable(false);
		setLocation(x, y);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
	}
}
