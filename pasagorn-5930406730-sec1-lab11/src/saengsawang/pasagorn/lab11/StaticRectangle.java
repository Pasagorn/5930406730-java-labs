package saengsawang.pasagorn.lab11;

import java.awt.Color;
import java.awt.geom.Rectangle2D;

public class StaticRectangle {
	Rectangle2D.Double rect;
	Color color;
	
	public StaticRectangle(Rectangle2D.Double rect, Color color) {
		super();
		this.rect = rect;
		this.color = color;
	}

	public Rectangle2D.Double getRect() {
		return rect;
	}

	public void setRect(Rectangle2D.Double rect) {
		this.rect = rect;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
}
