package saengsawang.pasagorn.lab8;

import java.awt.ItemSelectable;
import java.awt.event.*;
import java.util.Arrays;

import javax.swing.*;

import saengsawang.pasagorn.lab6.*;

/**
 * PatientFormV6
 * 
 * This program is to show the information of the patient in GUI and is able to
 * show the information the user put in
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */
public class PatientFormV4 extends PatientFormV3 implements ActionListener, ItemListener {

	public void addListener() {
		okB.addActionListener(this);
		cancelB.addActionListener(this);
		patientList.addActionListener(this);
		femaleBtn.addItemListener(this);
		maleBtn.addItemListener(this);
	}

	public PatientFormV4(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PatientFormV4 patientForm4 = new PatientFormV4("Patient Form V4");
		patientForm4.addComponents();
		patientForm4.addListener();
		patientForm4.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String selectedType = (String) patientList.getSelectedItem().toString();
		Object clickedBtn = e.getSource();
		JOptionPane optionPane = new JOptionPane("");

		// message to show when clibk OK
		if (clickedBtn == okB) {
			JOptionPane.showMessageDialog(this,
					"Name = " + nameText.getText() + " = Birthdate " + birthDateText.getText() + " Weight = "
							+ weightText.getText() + " Height = " + heightText.getText() + "\nGender = " + genderCheck()
							+ "\nAddress = " + addressText.getText() + "\nType = " + selectedType,
					"Message", JOptionPane.PLAIN_MESSAGE);
		}

		// clear the label when click cancel
		else if (clickedBtn == cancelB) {
			nameText.setText("");
			birthDateText.setText("");
			weightText.setText("");
			heightText.setText("");
			addressText.setText("");
		}

		// message show when patient type is changed
		else if (clickedBtn == patientList) {
			JOptionPane.showMessageDialog(this, "Your patient type is now change to " + selectedType, "Message",
					JOptionPane.PLAIN_MESSAGE);
		}
	}

	// check the radio button box and get value
	public String genderCheck() {
		if (maleBtn.isSelected()) {
			return "MALE";
		} else if (femaleBtn.isSelected()) {
			return "FEMALE";
		}
		return null;
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		ItemSelectable src = e.getItemSelectable();

		// message show when gender is changed
		if (((JRadioButton) src).isSelected()) {
			if (src == maleBtn) {
				JOptionPane optionPane = new JOptionPane("Your gender type is now change to " + genderCheck());
				JDialog dialog = optionPane.createDialog(this, "Gender Info");
				dialog.setLocation(getX() + 5, getY() + getHeight() + 20);
				dialog.setVisible(true);
			} else if (src == femaleBtn) {

				JOptionPane optionPane = new JOptionPane("Your gender type is now change to " + genderCheck());
				JDialog dialog = optionPane.createDialog(this, "Gender Info");
				dialog.setLocation(getX() + 5, getY() + getHeight() + 20);
				dialog.setVisible(true);
			}

		}

	}

}
