package saengsawang.pasagorn.lab8;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * PatientFormV5
 * 
 * This program is to show the information of the patient in GUI and is able to
 * show the information the user put in
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */
public class PatientFormV5 extends PatientFormV4 implements ActionListener {
	protected JMenuItem colorRed, colorGreen, colorBlue, size16, size20, size24, sizeCus;

	public PatientFormV5(String title) {
		super(title);

	}

	public void addListener() {
		super.addListener();
		colorRed.addActionListener(this);
		colorGreen.addActionListener(this);
		colorBlue.addActionListener(this);
		size16.addActionListener(this);
		size20.addActionListener(this);
		size24.addActionListener(this);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV5 patientForm5 = new PatientFormV5("Patient Form V5");
		patientForm5.addComponents();
		patientForm5.addListener();
		patientForm5.setFrameFeatures();
	}

	protected void addComponents() {
		super.addComponents();

		colorBlue = new JMenuItem("Blue");
		colorMI.add(colorBlue);
		colorGreen = new JMenuItem("Green");
		colorMI.add(colorGreen);
		colorRed = new JMenuItem("Red");
		colorMI.add(colorRed);

		size16 = new JMenuItem("16");
		sizeMI.add(size16);
		size20 = new JMenuItem("20");
		sizeMI.add(size20);
		size24 = new JMenuItem("24");
		sizeMI.add(size24);
		sizeCus = new JMenuItem("Custom...");
		sizeMI.add(sizeCus);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object src = e.getSource();
		setColor(src);
		setSize(src);

	}

	protected void setColor(Object src) {
		if (src == colorRed) {
			nameText.setForeground(Color.RED);
			birthDateText.setForeground(Color.RED);
			weightText.setForeground(Color.RED);
			heightText.setForeground(Color.RED);
			addressText.setForeground(Color.RED);
		} else if (src == colorGreen) {
			nameText.setForeground(Color.GREEN);
			birthDateText.setForeground(Color.GREEN);
			weightText.setForeground(Color.GREEN);
			heightText.setForeground(Color.GREEN);
			addressText.setForeground(Color.GREEN);
		} else if (src == colorBlue) {
			nameText.setForeground(Color.BLUE);
			birthDateText.setForeground(Color.BLUE);
			weightText.setForeground(Color.BLUE);
			heightText.setForeground(Color.BLUE);
			addressText.setForeground(Color.BLUE);
		}
	}

	protected void setSize(Object src) {
		Font font16 = new Font("Verdana", Font.PLAIN, 16);
		Font font20 = new Font("Verdana", Font.PLAIN, 20);
		Font font24 = new Font("Verdana", Font.PLAIN, 24);
		if (src == size16) {
			System.out.println("16");
			nameText.setFont(font16);
			birthDateText.setFont(font16);
			weightText.setFont(font16);
			heightText.setFont(font16);
			addressText.setFont(font16);
		} else if (src == size20) {
			nameText.setFont(font20);
			birthDateText.setFont(font20);
			weightText.setFont(font20);
			heightText.setFont(font20);
			addressText.setFont(font20);
		} else if (src == size24) {
			nameText.setFont(font24);
			birthDateText.setFont(font24);
			weightText.setFont(font24);
			heightText.setFont(font24);
			addressText.setFont(font24);
		}
	}

}
