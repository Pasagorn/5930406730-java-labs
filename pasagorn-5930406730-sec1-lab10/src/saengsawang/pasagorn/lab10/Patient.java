package saengsawang.pasagorn.lab10;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class Patient {
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
	
	String name, gender;
	LocalDate birthDate;
	double weight, height;
	
	public Patient(String name, String birthDate, String gender, double weight, int height) {
		this.name = name;
		this.birthDate = LocalDate.parse(birthDate, germanFormatter);
//		this.birthDate = birthDate;
		this.gender = gender;
		this.weight = weight;
		this.height = height;
	}

	@Override
	public String toString() {
		return "Patient [" + name + ", " 
				+ birthDate + ", " + gender + ", "+ weight + " kg., " + height + "cm.]";
	}
	
	public double getWeight() {
		return weight;
	}
	
	public String getName() {
		return name;
	}

}
