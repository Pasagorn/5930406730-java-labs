package saengsawang.pasagorn.lab10;

import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.SwingUtilities;

/**
 * PatientFormV11
 * 
 * This program is able to sort patients from their weight
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */

public class PatientFormV11 extends PatientFormV10 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PatientFormV11(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV11 patientForm11 = new PatientFormV11("Patient Form V11");
		patientForm11.addComponents();
		patientForm11.setFrameFeatures();
		patientForm11.addListener();
		patientForm11.setShortcut();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object src = e.getSource();
		if (src == sortMI) {
			sortPatients();
			displayPatient();
		}
	}

	@Override
	public void addListener() {
		super.addListener();
		sortMI.addActionListener(this);
	}

	// sort data from weight
	protected void sortPatients() {
		Collections.sort(data, PatientFormV11.WeightComparator);
	}

	public static Comparator<Patient> WeightComparator = new Comparator<Patient>() {
		public int compare(Patient p1, Patient p2) {
			return (int) (p1.getWeight() - p2.getWeight());
		}
	};

}
