package saengsawang.pasagorn.lab6;

import java.awt.*;
import javax.swing.*;

/**
 * PatientFormV3
 * 
 * This program is to show the use of GUI
 * 
 * @author Pasagorn Saengsawang
 * 
 * @version 1.0
 * 
 *          25-02-2017
 * 
 */

public class PatientFormV2 extends PatientFormV1 {

	protected JRadioButton maleBtn = new JRadioButton("Male");
	protected JRadioButton femaleBtn = new JRadioButton("Female");
	JLabel genderLabel = new JLabel("Gender: ");
	JLabel addressLabel = new JLabel("Address: ");

	protected JTextArea addressText = new JTextArea(2, 10);
	JScrollPane addressScroll = new JScrollPane(addressText);

	ButtonGroup group = new ButtonGroup();
	JPanel genderPanel = new JPanel();
	JPanel addressPanel = new JPanel();

	public PatientFormV2(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		PatientFormV2 patientForm2 = new PatientFormV2("Patient Form V2");
		patientForm2.addComponents();
		patientForm2.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void addComponents() {
		super.addComponents();
		genderPanel.add(maleBtn);
		genderPanel.add(femaleBtn);

		group.add(maleBtn);
		group.add(femaleBtn);

		//add gender panel to insert panel
		insetSection.add(genderLabel);
		insetSection.add(genderPanel);

		addressPanel.setLayout(new BorderLayout(0, 8));
		addressPanel.add(addressLabel, BorderLayout.NORTH);
		addressPanel.add(addressScroll, BorderLayout.CENTER);

		addressText.setLineWrap(true);
		addressText.setWrapStyleWord(true);
		addressText.setText(
				"Department of Computer Engineering, Facuty of Enginering, Khon Kaen University, Mittraparp Rd., T.Naimuang, A.Muang, Khon Kaen Province");

		//add gender panel to insert panel
		window.add(addressPanel);
	}

}
