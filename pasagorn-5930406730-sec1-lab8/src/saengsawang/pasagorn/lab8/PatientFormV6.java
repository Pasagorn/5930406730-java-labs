package saengsawang.pasagorn.lab8;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * PatientFormV6
 * 
 * This program is to show the information of the patient in GUI and is able to
 * show the information the user put in
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */

public class PatientFormV6 extends PatientFormV5 {

	private static final long serialVersionUID = 1L;

	protected JPanel windowV6, imagePane;
	protected JLabel imageLabel;

	public PatientFormV6(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV6 patientForm6 = new PatientFormV6("Patient Form V6");
		patientForm6.addComponents();
		patientForm6.addListener();
		patientForm6.setFrameFeatures();
	}

	protected void addComponents() {
		super.addComponents();

		// add image
		windowV6 = new JPanel();
		imagePane = new JPanel();
		imageLabel = new JLabel(new ImageIcon("images/manee.jpg"));
		imagePane.add(imageLabel);
		windowV6.setLayout(new BorderLayout());
		imagePane.setSize(window.getWidth(), window.getWidth());
		windowV6.add(imagePane, BorderLayout.NORTH);
		windowV6.add(window, BorderLayout.SOUTH);

		// remove new menu item and set icon image
		fileM.remove(newMI);
		openMI.setIcon(new ImageIcon("images/openIcon.png"));
		saveMI.setIcon(new ImageIcon("images/saveIcon.png"));
		exitMI.setIcon(new ImageIcon("images/quitIcon.png"));

		add(windowV6);
	}

}
