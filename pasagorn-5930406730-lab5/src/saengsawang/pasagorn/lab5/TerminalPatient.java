package saengsawang.pasagorn.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import saengsawang.pasagorn.lab4.Gender;

public class TerminalPatient extends PatientV2 {
	private String terminalDisease;
	private LocalDate firstDiagnosed;

	

	public TerminalPatient(String name, String birthDate, Gender gender, double weight, double height, String terminalDisease, String firstDiagnosed) {
		super(name, birthDate, gender, weight, height);
		this.terminalDisease = terminalDisease;
		this.firstDiagnosed = LocalDate.parse(firstDiagnosed, germanFormatter);
	}

	public String getTerminalDisease() {
		return terminalDisease;
	}

	public void setTerminalDisease(String terminalDisease) {
		this.terminalDisease = terminalDisease;
	}

	public LocalDate getFirstDiagnosed() {
		return firstDiagnosed;
	}

	public void setFirstDiagnosed(LocalDate firstDiagnosed) {
		this.firstDiagnosed = firstDiagnosed;
	}

	@Override
	public String toString() {
		return "TerminalPatient [" + terminalDisease + ", " + firstDiagnosed + "], " + super.toString();
	}

	@Override
	public void patientReport() {
		System.out.println("You have terminal illness.");
	}
}
