package saengsawang.pasagorn.lab5;

public interface HasPet {
	public void feedPet();

	public void playWithPet();

}
