package saengsawang.pasagorn.lab5;

public interface UnderLegalAge {
	public void askPermission();

	public void askPermission(String legalGuardianName);

}
