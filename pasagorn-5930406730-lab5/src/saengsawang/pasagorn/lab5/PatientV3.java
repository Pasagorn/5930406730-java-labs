package saengsawang.pasagorn.lab5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import saengsawang.pasagorn.lab4.Gender;

public abstract class PatientV3 extends SickPeople {

	protected DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
			.withLocale(Locale.GERMAN);

	private LocalDate birthDate;
	protected static Gender gender;
	private double weight;

	protected static double height;
	protected String name;

	// display that the patient has gone see the doctor
	public void seeDoctor() {
		System.out.println("go see the doctor.");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthdate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public PatientV3(String name, String birthDate, Gender gender, double weight, double height) {
		this.name = name;
		this.birthDate = LocalDate.parse(birthDate, germanFormatter);
		this.gender = gender;
		this.weight = weight;
		this.height = height;
	}

	public String toString() {
		return "Patient [" + name + ", " + birthDate + ", " + gender + ", " + weight + " kg., " + height + " cm.]";
	}

	public void patientReport() {
		// TODO Auto-generated method stub

	}

}
