package saengsawang.pasagorn.lab5;

import saengsawang.pasagorn.lab4.Gender;

public class AccidentPatientV2 extends PatientV3 implements HasInsurance, UnderLegalAge {

	private String typeOfAccident;
	private Boolean isInICU;

	public AccidentPatientV2(String name, String birthDate, Gender gender, double weight, double height,
			String typeOfAccident, boolean isInICU) {
		super(name, birthDate, gender, weight, height);
		this.typeOfAccident = typeOfAccident;
		this.isInICU = isInICU;
	}

	// display that the patient has paid the bill
	public void pay() {
		System.out.println("pay the accident bill with insurance.");
	}

	// display that the patient has asked pararent for permission
	public void askPermission() {
		System.out.println("ask pararent for permission to cure him in an accident.");
	}

	String getTypeOfAccident() {
		return typeOfAccident;
	}

	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}

	public Boolean getIsInICU() {
		return isInICU;
	}

	public void setIsInICU(Boolean isInICU) {
		this.isInICU = isInICU;
	}

	@Override
	public String toString() {
		return "AccidentPatient [" + typeOfAccident + ", " + " In ICU " + "], " + super.toString();
	}

	@Override
	public void patientReport() {
		System.out.println("You are in an accident");
	}

	public boolean isInICU() {
		if (this.getIsInICU())
			return true;
		else
			return false;
	}

}
