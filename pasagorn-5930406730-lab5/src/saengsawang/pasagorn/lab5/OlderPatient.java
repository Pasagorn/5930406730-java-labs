package saengsawang.pasagorn.lab5;

import java.time.LocalDate;

import saengsawang.pasagorn.lab4.Gender;

public class OlderPatient {

	public static void main(String[] args) {
		PatientV2 piti = new AccidentPatient("piti", "12.01.2000", Gender.MALE, 65.5, 169, "Car accident", true);
		PatientV2 weera = new TerminalPatient("weera", "15.02.2000", Gender.MALE, 72, 172, "Cancer", "01.01.2017");
		PatientV2 duangjai = new VIPPatient("duangjai", "21.05.2001", Gender.FEMALE, 47.5, 154, 1000000, "mickeymouse");
		PatientV2 petch = new AccidentPatient("petch", "15.10.1999", Gender.MALE, 60, 170, "Fire Accident", true);
		duangjai.setName(((VIPPatient) duangjai).getVIPName("mickeymouse"));
		
		isOlder(piti, weera);
		isOlder(piti, duangjai);
		isOlder(piti, petch);
		isOldest(piti, weera, duangjai);
		isOldest(piti, duangjai, petch);

	}

	static void isOlder(PatientV2 first, PatientV2 second) {
		if (first.getBirthDate().isBefore(second.getBirthDate())) {
			System.out.println(first.getName() + " is Older than " + second.getName());
		} else
			System.out.println(second.getName() + " is Older than " + first.getName());
	}

	static void isOldest(PatientV2 first, PatientV2 second, PatientV2 third) {
		PatientV2 max;
		if(first.getBirthDate().isBefore(second.getBirthDate())) {
			max = first;
		}		
		else {
			max = second;
		}
		if(third.getBirthDate().isBefore(max.getBirthDate())) {
			
			max = third;
		}
		System.out.println( max.getName() + " is the oldest among " + first.getName() + ", " + second.getName() + " and " + third.getName());
	}
	
}
