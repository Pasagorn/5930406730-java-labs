package saengsawang.pasagorn.lab5;

import saengsawang.pasagorn.lab4.Gender;
import saengsawang.pasagorn.lab4.*;

public class PatientV2 extends Patient {
	
	public PatientV2(String name, String birthDate, Gender gender, double weight, double height) {
		super(name, birthDate, gender, weight, height);
		// TODO Auto-generated constructor stub
	}

	public void patientReport() {
		System.out.println("You will need medical attention.");
	}

}
