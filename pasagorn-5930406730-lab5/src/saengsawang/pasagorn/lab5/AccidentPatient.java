package saengsawang.pasagorn.lab5;

import saengsawang.pasagorn.lab4.Gender;

public class AccidentPatient extends PatientV2{

	private String typeOfAccident;
	private	Boolean isInICU;
	
	public AccidentPatient(String name, String birthDate, Gender gender, double weight, double height, String typeOfAccident, boolean isInICU) {
		super(name, birthDate, gender, weight, height);
		this.typeOfAccident = typeOfAccident;
		this.isInICU = isInICU;
	}



	String getTypeOfAccident() {
		return typeOfAccident;
	}
	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}
	public Boolean getIsInICU() {
		return isInICU;
	}
	public void setIsInICU(Boolean isInICU) {
		this.isInICU = isInICU;
	}
	
	@Override
	public String toString() {
		return "AccidentPatient [" + typeOfAccident + ", " + " In ICU " + "], " + super.toString();
	}
	
	@Override
	public void patientReport() {
		System.out.println("You are in an accident");
	}



	public boolean isInICU() {
		if(this.getIsInICU()) return true;
		else return false;
	}
	
}
