package saengsawang.pasagorn.lab5;

import saengsawang.pasagorn.lab4.Gender;

public class VIPPatient extends PatientV2 {
	private double totalDonation;
	private String passPhrase;

	public VIPPatient(String name, String birthDate, Gender gender, double weight, double height, double totalDonation,
			String passPhrase) {
		super(encrypt(name), birthDate, gender, weight, height);
		this.totalDonation = totalDonation;
		this.passPhrase = passPhrase;
	}

	public double getTotalDonation() {
		return totalDonation;
	}

	public void setTotalDonation(double totalDonation) {
		this.totalDonation = totalDonation;
	}

	public void donate(double donate) {
		totalDonation += donate;
	}

	private static String encrypt(String message) {
		char[] name = message.toCharArray();
		for (int i = 0; i < name.length; i++) {
			name[i] = (char) ('a' + ((name[i]) + 14) % 26);
		}
		String enName = null;
		enName = String.valueOf(name);
		return enName;
	}

	private static String decrypt(String message) {
		char[] name = message.toCharArray();
		for (int i = 0; i < name.length; i++) {
			name[i] = (char) ('a' + (name[i]) % 26);
		}
		String deName = null;
		deName = String.valueOf(name);
		return deName;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	public String getVIPName(String passPhrase) {
		if (passPhrase.equals(this.passPhrase)) {
			return decrypt(name);
		} else
			return "Wrong passphrase, please try again";

	}

	@Override
	public String toString() {
		return "VIPPatient [" + totalDonation + ", Petient [" + super.toString().substring(9);
	}

	@Override
	public void patientReport() {
		System.out.println("Your record is private.");
	}

}
