package saengsawang.pasagorn.lab4;

/*
 * KhonKaenPatients
 * 
 * This program is to illustrate the examples of how to create object in
 * class Patient
 * 
 * @author Pasagorn Saengsawang
 * 
 * @version 1.0
 * 
 * 01-02-2017
 * 
 */

public class KhonKaenPatientsV2 {
	
	public static void main(String[] args) {
		OutPatient chujai = new OutPatient("Chujai", "02.06.1995", Gender.FEMALE, 52.7, 150);
		OutPatient piti = new OutPatient("Piti", "13.08.1995", Gender.MALE, 52.7, 150, "21.01.2017");
		chujai.setVisitDate("25.01.2017");
		System.out.println(chujai);
		System.out.println(piti);
		chujai.displayDaysBetween(piti);
		System.out.println("Both of them went to " + OutPatient.hospitalName + " hospital.");
	
	
		
	}

}
