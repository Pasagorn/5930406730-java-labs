package saengsawang.pasagorn.lab4;

import java.time.LocalDate;

public class InPatient extends Patient {
	private LocalDate admitDate, dischargeDate;

	public void setDischargeDate(LocalDate dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	public LocalDate getAdmitDate() {
		return admitDate;
	}

	public void setAdmitDate(String admitDate) {
		this.admitDate = LocalDate.parse(admitDate, germanFormatter);
	}

	public LocalDate getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(String dischargeDate) {
		this.dischargeDate = LocalDate.parse(dischargeDate, germanFormatter);
	}

	public InPatient(String name, String birthDate, Gender gender, double weight, double height, String admitDate,
			String dischargeDate) {
		super(name, birthDate, gender, weight, height);
		this.admitDate = LocalDate.parse(admitDate, germanFormatter);
		this.dischargeDate = LocalDate.parse(dischargeDate, germanFormatter);
	}

	public InPatient(String name, String birthDate, Gender gender, double weight, double height) {
		super(name, birthDate, gender, weight, height);
	}

	public InPatient(String name, String birthDate, Gender gender, double weight, double height, String admitDate) {
		super(name, birthDate, gender, weight, height);
		this.admitDate = LocalDate.parse(admitDate, germanFormatter);
	}

	@Override
	public String toString() {
		int length = super.toString().length();
		String mid = super.toString().substring(8, length-2);
		return "InPateint " + mid + " admitDate=" + admitDate + ", dischargeDate=" + dischargeDate + "]";
	}
	
	

	

}
