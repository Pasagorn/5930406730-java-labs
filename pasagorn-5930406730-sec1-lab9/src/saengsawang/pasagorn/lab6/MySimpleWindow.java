package saengsawang.pasagorn.lab6;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MySimpleWindow extends JFrame {

	protected JPanel buttonSection = new JPanel();
	protected JButton cancelB = new JButton("Cancel");
	protected JButton okB = new JButton("OK");

	public MySimpleWindow(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	protected void addComponents() {
		buttonSection.add(cancelB);
		buttonSection.add(okB);
		this.add(buttonSection);
	}

	public void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setVisible(true);
		pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
