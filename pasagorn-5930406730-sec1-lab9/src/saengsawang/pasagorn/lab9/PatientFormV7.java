package saengsawang.pasagorn.lab9;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import saengsawang.pasagorn.lab8.PatientFormV6;

/**
 * PatientFormV7
 * 
 * This program is to show the information of the patient in GUI and is able to
 * show the information the user put in
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 1
 * 
 * @version 1.0
 * 
 */
public class PatientFormV7 extends PatientFormV6 implements ChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JPanel BPPane, genderTextPane; 
	protected JSlider topNum, buttomNum;
	protected final int MIN = 0; 
	protected final int MAX = 200; 
	protected JLabel topLabel, bottomLabel;
	protected int topValue = MAX/2, bottomValue = MAX/2;
	protected TitledBorder BPTitle;
	protected JMenuItem colorCus, newMI;

	public PatientFormV7(String title) {
		super(title);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		PatientFormV7 patientForm7 = new PatientFormV7("Patient Form V7");
		patientForm7.addComponents();
		patientForm7.addListener();
		patientForm7.setFrameFeatures();
	}
	
	public void addComponents() {
		super.addComponents();
		
		JPanel buttonType = new JPanel();
		topNum = new JSlider(JSlider.HORIZONTAL, MIN, MAX, 100);
		topNum.setMajorTickSpacing(50);
		topNum.setMinorTickSpacing(10);
		topNum.setPaintTicks(true);
		topNum.setPaintLabels(true);
		
		buttomNum = new JSlider(JSlider.HORIZONTAL, MIN, MAX, 100);
		buttomNum.setMajorTickSpacing(50);
		buttomNum.setMinorTickSpacing(10);
		buttomNum.setPaintTicks(true);
		buttomNum.setPaintLabels(true);
		
		topLabel = new JLabel("Top number:");
		bottomLabel = new JLabel("Bottom Number:");
		
		BPPane = new JPanel();
		BPPane.setLayout(new GridLayout(0, 2));
		BPPane.add(topLabel);
		BPPane.add(topNum);
		BPPane.add(bottomLabel);
		BPPane.add(buttomNum);
		
		BPPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(new Color (176,224,230)), "Blood Pressure"));
		
		addressPanel.add(BPPane, BorderLayout.NORTH);
		addressPanel.add(addressLabel, BorderLayout.CENTER);
		addressPanel.add(addressScroll, BorderLayout.SOUTH);
		
		buttonType.setLayout(new BorderLayout());
		buttonType.add(typePanel, BorderLayout.NORTH);
		buttonType.add(buttonSection, BorderLayout.CENTER);
		
		this.add(buttonType, BorderLayout.SOUTH);

		colorCus = new JMenuItem("Custom...");
		colorMI.add(colorCus);
		
		newMI = new JMenuItem("New");
		fileM.add(newMI,0);
		
	}
	
	public void addListener() {
		super.addListener();
		ChangeListener l = null;
		topNum.addChangeListener(this);
		buttomNum.addChangeListener(this);

	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
	    JSlider src = (JSlider)e.getSource();
	   
	    if (!src.getValueIsAdjusting()) {
	    	if (src == topNum) {
	    		JOptionPane.showMessageDialog(this, "Top number of Blood Pressure is " + src.getValue());
	    		topValue = src.getValue();
	    	}
	    	 
	    	else {
	    		JOptionPane.showMessageDialog(this, "Bottom number of Blood Pressure is " + src.getValue());
	    		bottomValue = src.getValue();
	    	}
	    		
	    }
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	//	super.actionPerformed(e);
		
		String selectedType = (String) patientList.getSelectedItem().toString();
		Object clickedBtn = e.getSource();
		JOptionPane optionPane = new JOptionPane("");
		setColor(clickedBtn);

		// message to show when click OK
		if (clickedBtn == okB) {
			JOptionPane.showMessageDialog(this,
					"Name = " + nameText.getText() + " = Birthdate " + birthDateText.getText() + " Weight = "
							+ weightText.getText() + " Height = " + heightText.getText() + "\nGender = " + genderCheck()
							+ "\nAddress = " + addressText.getText() + "\nType = " + selectedType
							+ "\nBlood Pressure : Top Number - " + topValue + ", Bottom Number - " + bottomValue,
					"Message", JOptionPane.PLAIN_MESSAGE);
		}

		// clear the label when click cancel
		else if (clickedBtn == cancelB) {
			nameText.setText("");
			birthDateText.setText("");
			weightText.setText("");
			heightText.setText("");
			addressText.setText("");
		}

		// message show when patient type is changed
		else if (clickedBtn == patientList) {
			JOptionPane.showMessageDialog(this, "Your patient type is now change to " + selectedType, "Message",
					JOptionPane.PLAIN_MESSAGE);
		}
	}
	
}
