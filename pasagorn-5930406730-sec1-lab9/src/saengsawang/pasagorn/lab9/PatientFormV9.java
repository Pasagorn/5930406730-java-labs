package saengsawang.pasagorn.lab9;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.*;

/**
 * PatientFormV9
 * 
 * This program is to show the information of the patient in GUI and is able to
 * show the information the user put in
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 1
 * 
 * @version 1.0
 * 
 */
public class PatientFormV9 extends PatientFormV8 {
	JFileChooser chooser;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PatientFormV9(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV9 patientForm9 = new PatientFormV9("Patient Form V9");
		patientForm9.addComponents();
		patientForm9.addListener();
		patientForm9.setFrameFeatures();
		patientForm9.setShortcut();
	}

	public void addListener() {
		super.addListener();
		openMI.addActionListener(this);
		saveMI.addActionListener(this);
		colorCus.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		chooser = new JFileChooser();
		Object src = e.getSource();

		// show open dialog when open menu is clicked
		if (src == openMI) {
			int returnVal = chooser.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(this, "You have open file " + chooser.getSelectedFile().getName());
			} else {
				JOptionPane.showMessageDialog(this, "Open command is cancel");
			}
		}

		// show save dialog when save menu is clicked
		if (src == saveMI) {
			int returnVal = chooser.showSaveDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(this, "You have save file " + chooser.getSelectedFile().getName());
			} else {
				JOptionPane.showMessageDialog(this, "Save command is cancel");
			}
		}

		// show color panel
		if (src == colorCus) {
			Color current = null;
			Color cusColor = new JColorChooser().showDialog(null, "Select a color", current);
			setCusColor(cusColor);
		}

	}

	//choose color from color panel
	protected void setCusColor(Color cusColor) {
		nameText.setForeground(cusColor);
		birthDateText.setForeground(cusColor);
		weightText.setForeground(cusColor);
		heightText.setForeground(cusColor);
		addressText.setForeground(cusColor);
	}
}
