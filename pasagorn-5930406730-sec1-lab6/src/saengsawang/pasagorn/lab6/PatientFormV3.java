package saengsawang.pasagorn.lab6;

import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * PatientFormV3
 * 
 * This program is to show the use of GUI
 * 
 * @author Pasagorn Saengsawang
 * 
 * @version 1.0
 * 
 *          25-02-2017
 * 
 */

public class PatientFormV3 extends PatientFormV2 {
	
	JMenuBar menuBar = new JMenuBar();
	JMenu fileM = new JMenu("File");
	JMenu configM = new JMenu("Config");
	
	JMenuItem newMI = new JMenuItem("New");
	JMenuItem openMI = new JMenuItem("Open");
	JMenuItem saveMI = new JMenuItem("Save");
	JMenuItem exitMI = new JMenuItem("Exit");
	JMenuItem colorMI = new JMenuItem("Color");
	JMenuItem sizeMI = new JMenuItem("Size");
	
	JPanel typePanel = new JPanel();
	
	JLabel typeLabel = new JLabel("Type: ");
	
	String[] patientType = { "Outpatient", "Inpatient" };
	
	JComboBox patientList = new JComboBox(patientType);

public PatientFormV3(String title) {
		super(title);
	}
	
	public static void createAndShowGUI() {
		PatientFormV3 patientForm3 = new PatientFormV3("Patient Form V3");
		patientForm3.addComponents();
		patientForm3.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	protected void addComponents() {
		super.addComponents();
		typePanel.setLayout(new GridLayout(1, 1));
		typePanel.add(typeLabel);
		typePanel.add(patientList);
		
		//add type panel to address panel
		addressPanel.add(typePanel, BorderLayout.SOUTH);
		
		menuBar.add(fileM);
		menuBar.add(configM);
		fileM.add(newMI);
		fileM.add(openMI);
		fileM.add(saveMI);
		fileM.add(exitMI);
		configM.add(colorMI);
		configM.add(sizeMI);
		
		//add menu bar
		setJMenuBar(menuBar);

		add(window);
	}

}
