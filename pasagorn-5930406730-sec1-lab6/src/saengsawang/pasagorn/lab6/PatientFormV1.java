package saengsawang.pasagorn.lab6;

import java.awt.*;
import javax.swing.*;

/**
 * PatientFormV3
 * 
 * This program is to show the use of GUI
 * 
 * @author Pasagorn Saengsawang
 * 
 * @version 1.0
 * 
 *          25-02-2017
 * 
 */

public class PatientFormV1 extends MySimpleWindow {
	JPanel insetSection = new JPanel();
	JPanel window = new JPanel();

	JLabel nameLabel = new JLabel("Name: ");
	JLabel birthDateLabel = new JLabel("Birth Date: ");
	JLabel weightLabel = new JLabel("Weight (kg.): ");
	JLabel heightLabel = new JLabel("Height (metre): ");

	JTextField nameText = new JTextField(20);
	JTextField birthDateText = new JTextField(20);
	JTextField weightText = new JTextField(20);
	JTextField heightText = new JTextField(20);

	public PatientFormV1(String title) {
		super(title);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	public static void createAndShowGUI() {
		PatientFormV1 patientForm1 = new PatientFormV1("Patient Form V1");
		patientForm1.addComponents();
		patientForm1.setFrameFeatures();
	}

	protected void addComponents() {

		super.addComponents();

		this.add(window);

		birthDateText.setToolTipText("ex. 22.02.2000");

		//insert section has all the labels and text fields
		insetSection.setLayout(new GridLayout(0, 2, 5, 5));
		insetSection.add(nameLabel);
		insetSection.add(nameText);
		insetSection.add(birthDateLabel);
		insetSection.add(birthDateText);
		insetSection.add(weightLabel);
		insetSection.add(weightText);
		insetSection.add(heightLabel);
		insetSection.add(heightText);

		//this is the biggest panel
		window.setLayout(new BorderLayout(5, 5));
		window.add(insetSection, BorderLayout.NORTH);
		window.add(buttonSection, BorderLayout.SOUTH);
	}

}
