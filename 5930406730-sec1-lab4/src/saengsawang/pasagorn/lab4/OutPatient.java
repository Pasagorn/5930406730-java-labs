package saengsawang.pasagorn.lab4;


import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

	

public class OutPatient extends Patient{	
	
	
	public static String hospitalName = "Srinakarin";	
	private LocalDate visitDate;
	
	public LocalDate getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(String visitDate) {
		this.visitDate = LocalDate.parse(visitDate, germanFormatter);
	}

	public OutPatient(String name, String birthDate, Gender gender, double weight, double height, String visitDate) {
		super(name, birthDate, gender, weight, height);
		this.visitDate = LocalDate.parse(visitDate, germanFormatter);
	}

	public OutPatient(String name, String birthDate, Gender gender, double weight, double height) {
		super(name, birthDate, gender, weight, height);
	}

	@Override
	public String toString() {		
		int length = super.toString().length(); //get toString from Patient
		String mid = super.toString().substring(8, length-2); ////cut string to put in the middle
		return "OutPateint " + mid + " visitdate = " + this.getVisitDate() + "]";
	}

	public void displayDaysBetween(OutPatient Another) {
		
		long daysBetween = ChronoUnit.DAYS.between(this.getVisitDate(), Another.getVisitDate());
		
		if(daysBetween > 0) {
			System.out.println(this.getName() + " visited after " + Another.getName() + " for "+ daysBetween + " days");
		}
		
		else {
			System.out.println(Another.getName() + " visited after " + this.getName() + " for "+ daysBetween*-1 + " days");
		}
	}
}
