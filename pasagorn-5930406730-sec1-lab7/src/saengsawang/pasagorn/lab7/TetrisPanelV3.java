package saengsawang.pasagorn.lab7;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

/**
 * 
 * 
 * to paint shape of Tetris
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */
public class TetrisPanelV3 extends TetrisPanelV2 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8944346686485142325L;
	private Thread running;
	private final int RECT_WIDTH = 60;
	private final int RECT_HEIGHT = 40;
	
	
	private final int SPEED = 3;
	private Rectangle2D.Double droppingRectangle;
	private int numNotMovingRect = 0;
	private ArrayList<Rectangle2D.Double> notMovingRect;
	private ArrayList<Color> notMovingRectColor;
	private int r, g, b;

	public TetrisPanelV3() {
		super();
		setBackground(Color.WHITE);
		notMovingRect = new ArrayList<>();
		notMovingRectColor = new ArrayList<>();
		posX = (WIDTH - RECT_WIDTH) / 2;
		posY = 0;

		r = (int) (Math.random() * 256);
		g = (int) (Math.random() * 256);
		b = (int) (Math.random() * 256);

	}

	
	public void paint(Graphics a) {
		super.paintComponent(a);
		Graphics2D g2 = (Graphics2D) a;

		for (int i = 0; i < numNotMovingRect; i++)
			if (numNotMovingRect > 0) {
				g2.setColor(notMovingRectColor.get(i));
				g2.fill(notMovingRect.get(i));
				g2.setColor(Color.BLACK);
				g2.draw(notMovingRect.get(i));
			}

		droppingRectangle = new Rectangle2D.Double(posX, posY, RECT_WIDTH, RECT_HEIGHT);
		g2.setColor(new Color(r, g, b));
		g2.fill(droppingRectangle);
		g2.setColor(Color.BLACK);
		g2.draw(droppingRectangle);

	}

	
	@Override
	public void run() {
		while (true) {
			if (posY + RECT_HEIGHT - 7 >= HEIGHT) {
				notMovingRect.add(droppingRectangle);
				notMovingRectColor.add(new Color(r, g, b));
				numNotMovingRect++;
				r = (int) (Math.random() * 256);
				g = (int) (Math.random() * 256);
				b = (int) (Math.random() * 256);
				posY = 0;
				posX = (int) (Math.random() * (WIDTH - RECT_WIDTH + 1));
			} else {
				posY += SPEED;
				repaint();
			}
			
			try {
				Thread.sleep(15);
			} catch (InterruptedException ex) {
			}
		}
	}
}
