package saengsawang.pasagorn.lab7;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

/**
 * 
 * 
 * to paint shape of Tetris
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */
public class TetrisPanel extends JPanel {
    final int WIDTH = 600;
    final int HEIGHT = 400;
    final int BLOCK_HORIZON = 30;
    final int BLOCK_VERTICAL = 20;
    final int SQUARE_WIDTH = WIDTH/BLOCK_HORIZON;
    final int SQUARE_HEIGHT = HEIGHT/BLOCK_VERTICAL;
    public TetrisPanel (){
        super();
        setBackground(Color.LIGHT_GRAY);
    }

    public Dimension getPreferredSize(){
        return new Dimension(WIDTH,HEIGHT);
    }

    public void paint (Graphics g){
        super.paintComponent(g);
        g.setFont(new Font(Font.SERIF,Font.BOLD,30));
        g.setColor(Color.red);
        int widthFont = g.getFontMetrics().stringWidth("Tetris");
        g.drawString("Tetris",(WIDTH-widthFont)/2,HEIGHT/4);
        TetrisShape zShape = new TetrisShape(Tetrimino.Z_SHAPE);
        TetrisShape sShape = new TetrisShape(Tetrimino.S_SHAPE);
        TetrisShape iShape = new TetrisShape(Tetrimino.I_SHAPE);
        TetrisShape tShape = new TetrisShape(Tetrimino.T_SHAPE);
        TetrisShape oShape = new TetrisShape(Tetrimino.O_SHAPE);
        TetrisShape lShape = new TetrisShape(Tetrimino.L_SHAPE);
        TetrisShape jShape = new TetrisShape(Tetrimino.J_SHAPE);
        paintShape(g,zShape,2,9);
        paintShape(g,sShape,8,9);
        paintShape(g,iShape,11,10);
        paintShape(g,tShape,15,9);
        paintShape(g,oShape,19,10);
        paintShape(g,lShape,23,9);
        paintShape(g,jShape,27,9);

    }

    public void paintShape(Graphics t,TetrisShape shape, int offSetX, int offSetY){				
        for(int i = 0; i < 4 ; i++){
            int x = (offSetX+shape.coordinate[i][0])*SQUARE_WIDTH;
            int y = (offSetY-shape.coordinate[i][1])*SQUARE_HEIGHT;
            t.setColor(shape.colorOfShape);
            t.fillRect(x,y,SQUARE_WIDTH,SQUARE_HEIGHT);
            t.setColor(Color.DARK_GRAY);
            t.drawRect(x,y,SQUARE_WIDTH,SQUARE_HEIGHT);
        }
    }




}
