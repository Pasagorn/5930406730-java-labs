package saengsawang.pasagorn.lab7;

import java.awt.Color;
import java.awt.Graphics;

/**
 * 
 * 
 * to paint shape of Tetris
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */
public class TetrisPanelV2 extends TetrisPanel implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3852379082091325868L;
	private Thread running;
	private final int RECT_WIDTH = 50;
	private final int RECT_HEIGHT = 50;
	private final int SPEED = 5;

	protected int posX, posY;

	public TetrisPanelV2() {
		super();
		setBackground(Color.BLACK);
		posY = 0;
		posX = (int) (Math.random() * (WIDTH - RECT_WIDTH));
		running = new Thread(this);
		running.start();
	}

	public void paint(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.YELLOW);
		g.fillRect(posX, posY, RECT_WIDTH, RECT_HEIGHT);
	}

	
	@Override
	public void run() {
		while (true) {

			if (posY - SPEED >= HEIGHT) {
				posY = 0;
				posX = (int) (Math.random() * (WIDTH - RECT_WIDTH + 1));
			}
			posY += SPEED;
			repaint();

			try {
				Thread.sleep(25);
			} catch (InterruptedException ex) {

			}
		}
	}

	protected Thread getRunning() {
		return running;
	}
}
