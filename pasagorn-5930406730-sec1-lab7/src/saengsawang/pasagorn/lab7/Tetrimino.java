package saengsawang.pasagorn.lab7;

import java.awt.Color;

/**
 * 
 * 
 * the shapes and coordinates of Tetris
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */
public enum Tetrimino {
    Z_SHAPE(new Color(255,0,0),0),
    S_SHAPE(new Color(0,255,0),1),
    I_SHAPE(new Color(0, 255, 255),2),
    T_SHAPE(new Color(170,0,255),3),
    O_SHAPE(new Color(255,255,0),4),
    L_SHAPE(new Color(255,165,0),5),
    J_SHAPE(new Color(0,0,255),6);

    public Color shapeColor;
    public int[][][] allShapeCoordinate = {
            { { 0, -1 },  { 0, 0 },   { 1, 0 },   { 1, 1 } },   //ZShape
            { { 0, -1 },  { 0, 0 },   { -1, 0 },  { -1, 1 } },  //SShape
            { { 0, -1 },  { 0, 0 },   { 0, 1 },   { 0, 2 } },   //IShape
            { { -1, 0 },  { 0, 0 },   { 1, 0 },   { 0, -1 } },  //TShape
            { { 0, 0 },   { 1, 0 },   { 0, 1 },   { 1, 1 } },   //OShape
            { { -1, 1 },  { 0, 0 },  { 0, 1 },   { 0, -1 } },   //LShape
            { { 1, 1 }, { 0, -1 },  { 0, 0 },   { 0, 1 } }      //JShape
    };

    public int[][] shapeCoordinate = new int[4][2];

    private Tetrimino(Color c, int index) {
        shapeColor = c;
            for (int i = 0; i < 4 ; i++) {											
                for (int j = 0; j < 2; ++j) {
                    shapeCoordinate[i][j] = allShapeCoordinate[index][i][j];
                }
            }
    }
}
