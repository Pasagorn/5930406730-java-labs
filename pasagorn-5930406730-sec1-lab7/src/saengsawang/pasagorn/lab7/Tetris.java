package saengsawang.pasagorn.lab7;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * TetrisV
 * 
 * This program show the shape of Tetrises
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */
public class Tetris extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4631903561354801907L;

	public Tetris(String setTitle) {
		super(setTitle);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected static void createAndShowGUI() {
		Tetris tetris = new Tetris("CoE Tetris Game");
		tetris.addComponents();
		tetris.setFrameFeatures();
	}

	protected void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setVisible(true);
		this.pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		this.setLocation(x, y);
		setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	protected void addComponents() {
		TetrisPanel tetrisPanel = new TetrisPanel();
		this.add(tetrisPanel);
	}
}
