package saengsawang.pasagorn.lab7;

import java.awt.Color;

/**
 * 
 * 
 * TetrisShape class
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */
public class TetrisShape {
	private Tetrimino shape;
	protected int[][] coordinate;
	protected Color colorOfShape;

	public TetrisShape(Tetrimino shape) {
		colorOfShape = shape.shapeColor;
		coordinate = shape.shapeCoordinate;
	}

	public Tetrimino getShape() { 
		return shape;
	}

	public void setShape(Tetrimino shape) { 
		this.shape = shape;
	}
}
