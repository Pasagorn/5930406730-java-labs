package saengsawang.pasagorn.lab7;

import javax.swing.SwingUtilities;

/**
 * TetrisV2
 * 
 * This program show the rectangle falling randomly
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */
public class TetrisV2 extends Tetris {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7700624228624431832L;

	public TetrisV2(String setTitle) { 
		super(setTitle);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected static void createAndShowGUI() {					
		TetrisV2 tetrisV2 = new TetrisV2("Rectangle Dropping");
		tetrisV2.addComponents();
		tetrisV2.setFrameFeatures();
	}

	@Override
	protected void addComponents() {							
		TetrisPanelV2 tetrisV2Panel = new TetrisPanelV2();
		this.add(tetrisV2Panel);
	}
}