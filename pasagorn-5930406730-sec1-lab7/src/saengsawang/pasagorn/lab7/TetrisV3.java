package saengsawang.pasagorn.lab7;

import javax.swing.SwingUtilities;

/**
 * TetrisV3
 * 
 * This program show the rectangle falling randomly and stop at the bottom
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */
public class TetrisV3 extends TetrisV2 {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4427641918945836489L;

	public TetrisV3(String setTitle) {
		super(setTitle);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected static void createAndShowGUI() { 				
		TetrisV3 tetrisV3 = new TetrisV3("Rectangle Dropping V2");
		tetrisV3.addComponents();
		tetrisV3.setFrameFeatures();
	}

	@Override
	protected void addComponents() { 						
		TetrisPanelV3 tetrisV3Panel = new TetrisPanelV3();
		this.add(tetrisV3Panel);
	}
}
