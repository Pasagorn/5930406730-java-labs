package saengsawang.pasagorn.lab9;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import saengsawang.pasagorn.lab8.PatientFormV6;

/**
 * PatientFormV8
 * 
 * This program is to show the information of the patient in GUI and is able to
 * show the information the user put in
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 1
 * 
 * @version 1.0
 * 
 */

public class PatientFormV8 extends PatientFormV7 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PatientFormV8(String title) {
		super(title);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		PatientFormV8 patientForm8 = new PatientFormV8("Patient Form V8");
		patientForm8.addComponents();
		patientForm8.addListener();
		patientForm8.setFrameFeatures();
		patientForm8.setShortcut();
	}
	
	public void setShortcut() {
		fileM.setMnemonic(KeyEvent.VK_F);
		configM.setMnemonic(KeyEvent.VK_C);
		newMI.setMnemonic(KeyEvent.VK_N);
		openMI.setMnemonic(KeyEvent.VK_O);
		saveMI.setMnemonic(KeyEvent.VK_S);
		exitMI.setMnemonic(KeyEvent.VK_X);
		colorMI.setMnemonic(KeyEvent.VK_L);
		sizeMI.setMnemonic(KeyEvent.VK_Z);
		colorBlue.setMnemonic(KeyEvent.VK_B);
		colorGreen.setMnemonic(KeyEvent.VK_G);
		colorRed.setMnemonic(KeyEvent.VK_R);
		colorCus.setMnemonic(KeyEvent.VK_U);
		size16.setMnemonic(KeyEvent.VK_6);
		size20.setMnemonic(KeyEvent.VK_0);
		size24.setMnemonic(KeyEvent.VK_4);
		sizeCus.setMnemonic(KeyEvent.VK_M);
		
		colorBlue.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
		newMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		openMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		saveMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		exitMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
		colorGreen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		colorRed.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		colorCus.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
		size16.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6, ActionEvent.CTRL_MASK));
		size20.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0, ActionEvent.CTRL_MASK));
		size24.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, ActionEvent.CTRL_MASK));
		sizeCus.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.CTRL_MASK));
		
		
	}

}
