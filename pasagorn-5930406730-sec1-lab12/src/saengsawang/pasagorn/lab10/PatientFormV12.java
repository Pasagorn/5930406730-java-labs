package saengsawang.pasagorn.lab10;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * PatientFormV12
 * 
 * This program is able to search and remove patients data
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */

public class PatientFormV12 extends PatientFormV11 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PatientFormV12(String titleName) {
		super(titleName);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV12 patientForm12 = new PatientFormV12("Patient Form V12");
		patientForm12.addComponents();
		patientForm12.setFrameFeatures();
		patientForm12.addListener();
		patientForm12.setShortcut();
	}

	@Override
	public void addListener() {
		super.addListener();
		searchMI.addActionListener(this);
		removeMI.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == searchMI) {
			String searchName = JOptionPane.showInputDialog("Please enter the patient name");
			searchPatient(searchName);
		} else if (src == removeMI) {
			String removeName = JOptionPane.showInputDialog("Please enter the patient name to remove");
			removePatient(removeName);
		}
	}

	// remove data
	protected void removePatient(String name) {
		Patient p;
		if ((p = patientRemover(name)) != null) {
			data.remove(p);
			displayPatient();
		}
	}

	// search data by name to remove
	protected Patient patientRemover(String name) {
		for (Patient namePatient : data) {
			if (namePatient.getName().contains(name)) {
				return namePatient;
			}
		}
		JOptionPane.showMessageDialog(this, name + " is not found");
		return null;
	}

	// search data
	protected Patient searchPatient(String name) {
		for (Patient namePatient : data) {
			if (namePatient.getName().contains(name)) {
				JOptionPane.showMessageDialog(this, namePatient);
				return namePatient;
			}
		}
		JOptionPane.showMessageDialog(this, name + " is not found");
		return null;
	}

}
