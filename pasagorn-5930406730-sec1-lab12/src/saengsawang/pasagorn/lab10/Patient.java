package saengsawang.pasagorn.lab10;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class Patient {
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);
	
	String name, gender;
	LocalDate birthDate;
	double weight, height;
	
	public Patient(String name, String birthDate, String gender, double weight, int height) {
		this.name = name;
		this.birthDate = LocalDate.parse(birthDate, germanFormatter);
//		this.birthDate = birthDate;
		this.gender = gender;
		this.weight = weight;
		this.height = height;
	}
	
	public Patient(String name, LocalDate birthDate, String gender, double weight, int height) {
		this.name = name;
		this.birthDate = birthDate;
//		this.birthDate = birthDate;
		this.gender = gender;
		this.weight = weight;
		this.height = height;
	}

	public Patient(String name, String birthDate, double weight, int height, String gender) {
		this.name = name;
		this.birthDate = LocalDate.parse(birthDate, germanFormatter);
//		this.birthDate = birthDate;
		this.gender = gender;
		this.weight = weight;
		this.height = height;
		// TODO Auto-generated constructor stub
	}

	public Patient(String name, String birthDate, double weight, int height, Gender gender) {
		this.name = name;
		this.birthDate = LocalDate.parse(birthDate, germanFormatter);
//		this.birthDate = birthDate;
		this.gender = gender.toString();
		this.weight = weight;
		this.height = height;
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Patient [" + name + ", " 
				+ birthDate + ", " + gender + ", "+ weight + " kg., " + height + "cm.]";
	}
	
	public double getWeight() {
		return weight;
	}
	
	public String getName() {
		return name;
	}

	public DateTimeFormatter getGermanFormatter() {
		return germanFormatter;
	}

	public void setGermanFormatter(DateTimeFormatter germanFormatter) {
		this.germanFormatter = germanFormatter;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	
}
