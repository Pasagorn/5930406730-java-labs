package saengsawang.pasagorn.lab10;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import saengsawang.pasagorn.lab9.PatientFormV9;

/**
 * PatientFormV10
 * 
 * This program is able to collect data in Arraylist
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */

public class PatientFormV10 extends PatientFormV9 {
	int i = 0;
	public ArrayList<Patient> data = new ArrayList();
	JMenuItem displayMI, sortMI, searchMI, removeMI;
	JMenu dataMI;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PatientFormV10(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV10 patientForm10 = new PatientFormV10("Patient Form V10");
		patientForm10.addComponents();
		patientForm10.addListener();
		patientForm10.setFrameFeatures();
		patientForm10.setShortcut();
	}

	@Override
	public void addComponents() {
		dataMI = new JMenu("Data");
		displayMI = new JMenuItem("Display");
		sortMI = new JMenuItem("Sort");
		searchMI = new JMenuItem("Search");
		removeMI = new JMenuItem("remove");
		super.addComponents();

		menuBar.add(dataMI);
		dataMI.add(displayMI);
		dataMI.add(sortMI);
		dataMI.add(searchMI);
		dataMI.add(removeMI);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		if (src == okB) {
			okHandler();
		} else if (src == displayMI) {
			displayPatient();
		}
	}

	public void addListener() {
		super.addListener();
		displayMI.addActionListener(this);

	}

	// data data to arraylist
	protected void addPatient() {

		Patient newPatient = new Patient(nameText.getText(), birthDateText.getText(), genderCheck(),
				Integer.parseInt(weightText.getText()), Integer.parseInt(heightText.getText()));
		data.add(newPatient);
		// Patient neo = new Patient("sd", "16.08.2016", "Male", 45, 159);
		System.out.println(data.get(i) + "" + i);
		i++;
	}
	
	@Override
	public void okHandler() {
		Patient newPatient = new Patient(nameText.getText(), birthDateText.getText(), genderCheck(),
				Integer.parseInt(weightText.getText()), Integer.parseInt(heightText.getText()));
		data.add(newPatient);
		// Patient neo = new Patient("sd", "16.08.2016", "Male", 45, 159);
		System.out.println(data.get(i) + "" + i);
		i++;
		JOptionPane.showMessageDialog(this,
				"Name = " + nameText.getText() + " = Birthdate " + birthDateText.getText() + " Weight = "
						+ weightText.getText() + " Height = " + heightText.getText() + "\nGender = " + genderCheck()
						+ "\nAddress = " + addressText.getText() + "\nType = " + selectedType
						+ "\nBlood Pressure : Top Number - " + topValue + ", Bottom Number - " + bottomValue,
				"Message", JOptionPane.PLAIN_MESSAGE);
	}

	protected void okClick() {

	}

	// display list of data
	protected void displayPatient() {
		String ans = "";
		for (int j = 0; j < data.size(); j++) {
			ans += j + 1 + ": " + data.get(j).toString() + "\n";
		}
		JOptionPane.showMessageDialog(this, ans);

		// );
	}
}
