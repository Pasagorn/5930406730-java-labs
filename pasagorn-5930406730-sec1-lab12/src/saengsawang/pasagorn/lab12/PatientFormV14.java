package saengsawang.pasagorn.lab12;

import java.awt.event.ActionEvent;
import java.time.format.DateTimeParseException;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import saengsawang.pasagorn.lab10.Gender;
import saengsawang.pasagorn.lab10.Patient;

/**
 * PatientFormV12
 * 
 * This program is able to search and remove patients data
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */

public class PatientFormV14 extends PatientFormV13 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PatientFormV14(String titleName) {
		super(titleName);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV14 patientForm14 = new PatientFormV14("Patient Form V14");
		patientForm14.addComponents();
		patientForm14.setFrameFeatures();
		patientForm14.addListener();
		patientForm14.setShortcut();
	}

	@Override
	public void okHandler() {
		try {
			String name = CheckPatient.checkName(nameText.getText());
			String birthDate = CheckPatient.checkBirthdate(birthDateText.getText());
			double weight = Double.parseDouble(weightText.getText());
			int height = Integer.parseInt(heightText.getText());
			Gender gender = CheckPatient.checkGender(maleBtn.isSelected(), femaleBtn.isSelected());
			CheckPatient.checkAddress(addressText.getText());

			Patient newPatient = new Patient(name, birthDate, weight, height, gender);
			data.add(newPatient);

			JOptionPane.showMessageDialog(this,
					"Name = " + nameText.getText() + " = Birthdate " + birthDateText.getText() + " Weight = "
							+ weightText.getText() + " Height = " + heightText.getText() + "\nGender = " + genderCheck()
							+ "\nAddress = " + addressText.getText() + "\nType = " + selectedType
							+ "\nBlood Pressure : Top Number - " + topValue + ", Bottom Number - " + bottomValue,
					"Message", JOptionPane.PLAIN_MESSAGE);

			//make the program robust
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Please enter weight in double and height in int");
		} catch (DateTimeParseException e) {
			JOptionPane.showMessageDialog(this, "Please enter Birthdate in the format DD.MM.YYYY ex. 22.02.2000");
		} catch (NoNameException e) {
			JOptionPane.showMessageDialog(this, "Name has not been entered.");
		} catch (NoGenderException e) {
			JOptionPane.showMessageDialog(this, "No gender has been selected.");
		} catch (NoAddressException e) {
			JOptionPane.showMessageDialog(this, "Address has not been entered.");
		} catch (NoBirthdateException e) {
			JOptionPane.showMessageDialog(this, "Please enter birthdate.");
		}

	}

}
