package saengsawang.pasagorn.lab12;

import saengsawang.pasagorn.lab10.Gender;

/**
 * Created by acer on 2/5/2560.
 */
public class CheckPatient {

    public static String checkName(String name) throws NoNameException {
        if (name.isEmpty()) throw new NoNameException();
        return name;
    }

    public static Gender checkGender(boolean male, boolean female) throws NoGenderException {
        if (!(male || female)) throw new NoGenderException();
        if (male) return Gender.MALE;
        else return Gender.FEMALE;
    }

    public static String checkAddress(String address) throws NoAddressException {
        if (address.isEmpty()) throw new NoAddressException();
        return address;
    }

    public static String checkBirthdate(String birthdate) throws NoBirthdateException {
        if (birthdate.isEmpty()) throw new NoBirthdateException();
        return birthdate;
    }
}
