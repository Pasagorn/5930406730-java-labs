package saengsawang.pasagorn.lab12;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.plaf.FileChooserUI;

import saengsawang.pasagorn.lab10.Patient;
import saengsawang.pasagorn.lab10.PatientFormV12;

/**
 * PatientFormV12
 * 
 * This program is able to search and remove patients data
 * 
 * @author Pasagorn Saengsawang id: 593040673-0 section 2
 * 
 * @version 1.0
 * 
 */

public class PatientFormV13 extends PatientFormV12{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected PrintStream fr;
	DateTimeFormatter dtformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public PatientFormV13(String titleName) {
		super(titleName);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV13 patientForm13 = new PatientFormV13("Patient Form V13");
		patientForm13.addComponents();
		patientForm13.setFrameFeatures();
		patientForm13.addListener();
		patientForm13.setShortcut();
	}
	
	@Override
	public void saveHandler(ActionEvent e) {
		chooser = new JFileChooser();
		Object src = e.getSource();

		// show open dialog when open menu is clicked
		if (src == openMI) {
			int returnVal = chooser.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(this, "You have open file " + chooser.getSelectedFile().getName());
				List<String> list = new ArrayList<String>();
				File file = chooser.getSelectedFile();
				BufferedReader reader = null;

				try {
				    reader = new BufferedReader(new FileReader(file));
				    String text = null;
				    String name = null, birthdate = null, weight = null, height = null, gender = null; 
				    
				    
				    while ((text = reader.readLine()) != null) {
				    	
				    			name = text;
				    			birthdate = reader.readLine();
				    			weight = reader.readLine();
				    			height = reader.readLine();
				    			gender = reader.readLine();
				    		
				    	Patient newPatient = new Patient(name.substring(7, name.length()), LocalDate.parse(birthdate.substring(12, birthdate.length()), dtformatter), gender.substring(9, gender.length()), Double.parseDouble(weight.substring(9, weight.length())), Integer.parseInt(height.substring(9, height.length()-2)));
				        data.add(newPatient);
				    	
				        //list.add(text);
				        //list.add("\n");
				        
				    }
				    displayPatient();
				   // JOptionPane.showMessageDialog(this, data);
				} catch (FileNotFoundException e1) {
				    e1.printStackTrace();
				} catch (IOException e1) {
				    e1.printStackTrace();
				} finally {
				    try {
				        if (reader != null) {
				            reader.close();
				        }
				    } catch (IOException e1) {
				    }
				}
				
				
			} else {
				JOptionPane.showMessageDialog(this, "Open command is cancel");
			}
		}

		// show save dialog when save menu is clicked
		if (src == saveMI) {
			int returnVal = chooser.showSaveDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(this, "You have save file " + chooser.getSelectedFile().getName());
			//	System.out.println(data.get(0).getName());
				
				File file = chooser.getSelectedFile();
				
				try {
					fr = new PrintStream(file);
					for (int i=0;i<data.size();i++) {
						fr.println("Name = " + data.get(i).getName());
						fr.println("Birthdate = " + data.get(i).getBirthDate());
						fr.println("Weight = " + data.get(i).getWeight());
						fr.println("Height = " + data.get(i).getHeight());
						fr.println("Gender = " + data.get(i).getGender());
					}
					
					
				} catch(IOException e1) {
					e1.printStackTrace();
				} finally {
					fr.close();
				}
				
			} else {
				JOptionPane.showMessageDialog(this, "Save command is cancel");
			}
		}

		// show color panel
		if (src == colorCus) {
			Color current = null;
			Color cusColor = new JColorChooser().showDialog(null, "Select a color", current);
			setCusColor(cusColor);
		}
	}

}
